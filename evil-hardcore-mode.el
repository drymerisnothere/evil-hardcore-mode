;;; evil-hardcore-mode.el --- hardcore-mode for evil
;; Author: drymer <drymer [ at ] autistici.org>
;; Version: 0.1
;; URL: https://git.daemons.it/drymer/evil-hardcore-mode
;;; Commentary:
;; Good practices for evil-mode. Heavyly based in hardcore-mode.
;;; Code:

(defgroup evil-hardcore-mode nil
  "Disable emacs default keybinds."
  :group 'keyboard)

(defvar evil-hardcore-mode-map nil
  "Keymap for evil hardcore Emacs minor mode.")

(defvar evil-hardcore-my-bad-keybinds nil)

(defvar evil-hardcore-bad-keybinds (append '("C-h" "<C-down>" "<C-left>"
                                             "<C-up>" "<C-right>" "M-v" "M-<"
                                             "M->" "C-x b" "C-x C-f" "C-x C-s"
                                             "M-TAB")
                                           evil-hardcore-my-bad-keybinds))

(defun evil-hardcore-disabled()
  "Say the user is not evil enough."
  (interactive)
  (message "You're not evil enough. Shame on you!"))

(if evil-hardcore-mode-map nil
  (setq evil-hardcore-mode-map (make-sparse-keymap))
  (dolist (key evil-hardcore-bad-keybinds)
    (define-key evil-hardcore-mode-map (kbd key) 'nil))
  (dolist (key evil-hardcore-bad-keybinds)
    (define-key evil-hardcore-mode-map (kbd key) 'evil-hardcore-disabled)))

;;##autoload
(define-minor-mode evil-hardcore-mode
  "Evil hardcore emacs minor mode."
  nil " ehc" evil-hardcore-mode-map)

;;##autoload
(define-globalized-minor-mode global-evil-hardcore-mode
  evil-hardcore-mode evil-hardcore-mode)

(provide 'evil-hardcore-mode)
;;; evil-hardcore-mode.el ends here
